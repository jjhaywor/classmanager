This document follows [semantic versioning](https://semver.org/)
# 0.1.1
## Modified
 - Put issue templates in .gitlab folder
# 0.1.0
## Added
 - Issue Templates #1

> Written with [StackEdit](https://stackedit.io/).