# Bug Summary
 - Insert one sentence summary here
 - Enter supporting data such as screenshot here

# Root Cause
Insert description of root cause once found

# Results
## Added
 - List item
## Modified
 - List item
## Removed
 - List item

# Testing
| **Date** | **Test** |
| :------: | :-------------------- |
| XX/XX/XX | Insert test here |

History
=========
| **Date** | **Forecast or Event** |
| :------: | :-------------------- |
| XX/XX/XX | Development Started |
| XX/XX/XX | Development Completed |
| XX/XX/XX | Development Abandoned <br> - Reason: ... |

> Written with [StackEdit](https://stackedit.io/).